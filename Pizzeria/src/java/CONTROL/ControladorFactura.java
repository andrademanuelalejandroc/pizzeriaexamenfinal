/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;


/**
 *
 * @author MANUEL
 */
@WebServlet(name = "ControladorFactura", urlPatterns = {"/ControladorFactura"})
public class ControladorFactura extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorFactura</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorFactura at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("accion");

        if (action.equalsIgnoreCase("calcular factura")) {
            try {
                calcularFactura(request, response);
            } catch (Exception ex) {
                Logger.getLogger(ControladorFactura.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    public String[][] leerIngredientes(HttpServletRequest request, HttpServletResponse response, int cantidadDePizzas, int pizza) {

        List<DTO.IngredienteAdicional> ingredientes = (List<DTO.IngredienteAdicional>) request.getSession().getAttribute("ingredientes");
        String[][] ingredientesPizza = new String[cantidadDePizzas][ingredientes.size()];

        for (int i = 0; i < cantidadDePizzas; i++) {

            if (request.getParameterValues("ingredientes" + (pizza) + "" + i) != null) {
                System.out.println("Valores" + request.getParameterValues("ingredientes" + (pizza) + "" + i));
                ingredientesPizza[i] = (String[]) request.getParameterValues("ingredientes" + (pizza) + "" + i);
            }
        }
        return ingredientesPizza;
    }

    public void calcularFactura(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {

        int cantidadDePizzas = Integer.parseInt(request.getSession().getAttribute("cantidadDePizza") + "");
        
        String tamanos[] = (String[]) request.getSession().getAttribute("tamanoPizzas");
        String[] saboresPrimeraPizza = request.getParameterValues("select1");
        String[] saboresSegundaPizza = request.getParameterValues("select2");
        String[][] ingredientesPrimeraPizza = leerIngredientes(request, response, cantidadDePizzas, 1);
        String[][] ingredientesSegundaPizza = leerIngredientes(request, response, cantidadDePizzas, 2);
        //PROCESOS DE FACTURACION
        NEGOCIO.SistemaFactura sistemaFactura = new NEGOCIO.SistemaFactura();
       //   JOptionPane.showMessageDialog(null, "ANTES");
        sistemaFactura.guardarDatos(saboresPrimeraPizza, saboresSegundaPizza, ingredientesPrimeraPizza, ingredientesSegundaPizza, tamanos);
        //JOptionPane.showMessageDialog(null, "DESPUES");
        sistemaFactura.calcularCostoPizza(tamanos, saboresPrimeraPizza, saboresSegundaPizza);
        // JOptionPane.showMessageDialog(null, "F");
        cargarFactura(request, response, sistemaFactura);
 
       // registrarAdicional(request, response, saboresPrimeraPizza, saboresSegundaPizza, ingredientesPrimeraPizza, ingredientesSegundaPizza);

        mostrar(request, response);
    }

    public void registrarAdicional(HttpServletRequest request, HttpServletResponse response, String[] saboresPrimeraPizza, String[] saboresSegundaPizza, String[][] ingredientesPrimeraPizza, String[][] ingredientesSegundaPizza) throws Exception {

        NEGOCIO.SistemaPizza sistema = new NEGOCIO.SistemaPizza();
        int idPedidos[] = sistema.obtenerIdPedidos(saboresPrimeraPizza.length);
        sistema.guardarIngredientes(saboresPrimeraPizza, ingredientesPrimeraPizza, idPedidos);
        sistema.guardarIngredientes(saboresSegundaPizza, ingredientesSegundaPizza, idPedidos);

    }

    public void cargarFactura(HttpServletRequest request, HttpServletResponse response, NEGOCIO.SistemaFactura sistema) {
        sistema.generarCadenaPizza();
        request.getSession().setAttribute("nombrePizzas", sistema.getNombreDeLaPizza());
        request.getSession().setAttribute("ingredientesPrimeraMitad", sistema.getIngredientesPrimeraMitad());
        request.getSession().setAttribute("ingredientesSegundaMitad", sistema.getIngredientesSegundaMitad());
        request.getSession().setAttribute("saboresPrimeraMitad", sistema.getSaboresPrimeraMitad());
        request.getSession().setAttribute("saboresSegundaMitad", sistema.getSaboresSegundaMitad());
        request.getSession().setAttribute("ingredientesPrimeraMitadCostos", sistema.getIngredientesPrimeraMitadCostos());
        request.getSession().setAttribute("ingredientesSegundaMitadCostos", sistema.getIngredientesSegundaMitadCostos());
        request.getSession().setAttribute("CostoPizzas", sistema.getCostoPizzas());
        request.getSession().setAttribute("total", sistema.getTotal() + "");

    }

    public void mostrar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("jsp/factura.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
