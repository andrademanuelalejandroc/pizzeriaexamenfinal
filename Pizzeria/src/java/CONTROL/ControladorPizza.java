/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import NEGOCIO.SistemaPizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MANUEL
 */
@WebServlet(name = "ControladorPizza", urlPatterns = {"/ControladorPizza"})
public class ControladorPizza extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorPizza</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorPizza at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("accion");

        if (action.equalsIgnoreCase("crear")) {
            crearPedido(request, response);
        }

        if (action.equalsIgnoreCase("actualizarIndex")) {
            actualizarIndex(request, response);
        }

        if (action.equalsIgnoreCase("Cargar opciones")) {
            cargarOpciones(request, response);
        }

    }

    public void mostrar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("jsp/factura.jsp");
    }

    public void cargarDescripciones(HttpServletRequest request, HttpServletResponse response, SistemaPizza sistema) {
        String[] descripciones = sistema.obtenerDescripcionSabores();
        request.getSession().setAttribute("descripciones", descripciones);
    }

    public void cargarImagenes(HttpServletRequest request, HttpServletResponse response, SistemaPizza sistema) {
        String[] imagenes = sistema.obtenerImagenes();
        request.getSession().setAttribute("imagenes", imagenes);
    }

    public void cargarSabores(HttpServletRequest request, HttpServletResponse response, SistemaPizza sistema) {
        List<DTO.Sabor> sabores = sistema.obtenerSabores();
        request.getSession().setAttribute("sabores", sabores);
    }

    public void cargarIngredientes(HttpServletRequest request, HttpServletResponse response, SistemaPizza sistema) {
        List<DTO.IngredienteAdicional> ingredientes = sistema.ObtenerIngredientes();
        request.getSession().setAttribute("ingredientes", ingredientes);
    }

    public void cargarCantidadDePizzas(HttpServletRequest request, HttpServletResponse response) {
        String[] tamano = request.getParameterValues("tama");
        request.getSession().setAttribute("tamanoPizzas", tamano);

    }

    public void cargarIdSabor(HttpServletRequest request, HttpServletResponse response, SistemaPizza sistema) {
        String[] idSabores = sistema.obtenerIdSabores();
        request.getSession().setAttribute("idSabores", idSabores);
    }

    public void cargarOpciones(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SistemaPizza sistema = new NEGOCIO.SistemaPizza();
        cargarDescripciones(request, response, sistema);
        cargarImagenes(request, response, sistema);
        cargarSabores(request, response, sistema);
        cargarIngredientes(request, response, sistema);
        cargarCantidadDePizzas(request, response);
        cargarIdSabor(request, response, sistema);
        response.sendRedirect("jsp/opciones.jsp");
    }

    public void actualizarIndex(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().removeAttribute("cantidadDePizza");
        response.sendRedirect("index.jsp");
    }

    public void crearPedido(HttpServletRequest request, HttpServletResponse response) throws IOException {

        NEGOCIO.SistemaPizza sistema = new NEGOCIO.SistemaPizza();
        int cantidad = Integer.parseInt(request.getParameter("cantidadPizzas"));
        request.getSession().setAttribute("cantidadDePizza", cantidad);
        request.getSession().setAttribute("tipos", sistema.obtenerTipos());
        response.sendRedirect("index.jsp");

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
