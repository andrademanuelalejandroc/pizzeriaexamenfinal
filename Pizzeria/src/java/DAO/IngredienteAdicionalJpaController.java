/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DTO.IngredienteAdicional;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.Pizzaadicional;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author MANUEL
 */
public class IngredienteAdicionalJpaController implements Serializable {

    public IngredienteAdicionalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(IngredienteAdicional ingredienteAdicional) {
        if (ingredienteAdicional.getPizzaadicionalList() == null) {
            ingredienteAdicional.setPizzaadicionalList(new ArrayList<Pizzaadicional>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Pizzaadicional> attachedPizzaadicionalList = new ArrayList<Pizzaadicional>();
            for (Pizzaadicional pizzaadicionalListPizzaadicionalToAttach : ingredienteAdicional.getPizzaadicionalList()) {
                pizzaadicionalListPizzaadicionalToAttach = em.getReference(pizzaadicionalListPizzaadicionalToAttach.getClass(), pizzaadicionalListPizzaadicionalToAttach.getPizzaadicionalPK());
                attachedPizzaadicionalList.add(pizzaadicionalListPizzaadicionalToAttach);
            }
            ingredienteAdicional.setPizzaadicionalList(attachedPizzaadicionalList);
            em.persist(ingredienteAdicional);
            for (Pizzaadicional pizzaadicionalListPizzaadicional : ingredienteAdicional.getPizzaadicionalList()) {
                IngredienteAdicional oldIngredienteAdicionalOfPizzaadicionalListPizzaadicional = pizzaadicionalListPizzaadicional.getIngredienteAdicional();
                pizzaadicionalListPizzaadicional.setIngredienteAdicional(ingredienteAdicional);
                pizzaadicionalListPizzaadicional = em.merge(pizzaadicionalListPizzaadicional);
                if (oldIngredienteAdicionalOfPizzaadicionalListPizzaadicional != null) {
                    oldIngredienteAdicionalOfPizzaadicionalListPizzaadicional.getPizzaadicionalList().remove(pizzaadicionalListPizzaadicional);
                    oldIngredienteAdicionalOfPizzaadicionalListPizzaadicional = em.merge(oldIngredienteAdicionalOfPizzaadicionalListPizzaadicional);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(IngredienteAdicional ingredienteAdicional) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngredienteAdicional persistentIngredienteAdicional = em.find(IngredienteAdicional.class, ingredienteAdicional.getIdIngrediente());
            List<Pizzaadicional> pizzaadicionalListOld = persistentIngredienteAdicional.getPizzaadicionalList();
            List<Pizzaadicional> pizzaadicionalListNew = ingredienteAdicional.getPizzaadicionalList();
            List<String> illegalOrphanMessages = null;
            for (Pizzaadicional pizzaadicionalListOldPizzaadicional : pizzaadicionalListOld) {
                if (!pizzaadicionalListNew.contains(pizzaadicionalListOldPizzaadicional)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pizzaadicional " + pizzaadicionalListOldPizzaadicional + " since its ingredienteAdicional field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Pizzaadicional> attachedPizzaadicionalListNew = new ArrayList<Pizzaadicional>();
            for (Pizzaadicional pizzaadicionalListNewPizzaadicionalToAttach : pizzaadicionalListNew) {
                pizzaadicionalListNewPizzaadicionalToAttach = em.getReference(pizzaadicionalListNewPizzaadicionalToAttach.getClass(), pizzaadicionalListNewPizzaadicionalToAttach.getPizzaadicionalPK());
                attachedPizzaadicionalListNew.add(pizzaadicionalListNewPizzaadicionalToAttach);
            }
            pizzaadicionalListNew = attachedPizzaadicionalListNew;
            ingredienteAdicional.setPizzaadicionalList(pizzaadicionalListNew);
            ingredienteAdicional = em.merge(ingredienteAdicional);
            for (Pizzaadicional pizzaadicionalListNewPizzaadicional : pizzaadicionalListNew) {
                if (!pizzaadicionalListOld.contains(pizzaadicionalListNewPizzaadicional)) {
                    IngredienteAdicional oldIngredienteAdicionalOfPizzaadicionalListNewPizzaadicional = pizzaadicionalListNewPizzaadicional.getIngredienteAdicional();
                    pizzaadicionalListNewPizzaadicional.setIngredienteAdicional(ingredienteAdicional);
                    pizzaadicionalListNewPizzaadicional = em.merge(pizzaadicionalListNewPizzaadicional);
                    if (oldIngredienteAdicionalOfPizzaadicionalListNewPizzaadicional != null && !oldIngredienteAdicionalOfPizzaadicionalListNewPizzaadicional.equals(ingredienteAdicional)) {
                        oldIngredienteAdicionalOfPizzaadicionalListNewPizzaadicional.getPizzaadicionalList().remove(pizzaadicionalListNewPizzaadicional);
                        oldIngredienteAdicionalOfPizzaadicionalListNewPizzaadicional = em.merge(oldIngredienteAdicionalOfPizzaadicionalListNewPizzaadicional);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ingredienteAdicional.getIdIngrediente();
                if (findIngredienteAdicional(id) == null) {
                    throw new NonexistentEntityException("The ingredienteAdicional with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngredienteAdicional ingredienteAdicional;
            try {
                ingredienteAdicional = em.getReference(IngredienteAdicional.class, id);
                ingredienteAdicional.getIdIngrediente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingredienteAdicional with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Pizzaadicional> pizzaadicionalListOrphanCheck = ingredienteAdicional.getPizzaadicionalList();
            for (Pizzaadicional pizzaadicionalListOrphanCheckPizzaadicional : pizzaadicionalListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This IngredienteAdicional (" + ingredienteAdicional + ") cannot be destroyed since the Pizzaadicional " + pizzaadicionalListOrphanCheckPizzaadicional + " in its pizzaadicionalList field has a non-nullable ingredienteAdicional field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(ingredienteAdicional);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<IngredienteAdicional> findIngredienteAdicionalEntities() {
        return findIngredienteAdicionalEntities(true, -1, -1);
    }

    public List<IngredienteAdicional> findIngredienteAdicionalEntities(int maxResults, int firstResult) {
        return findIngredienteAdicionalEntities(false, maxResults, firstResult);
    }

    private List<IngredienteAdicional> findIngredienteAdicionalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(IngredienteAdicional.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public IngredienteAdicional findIngredienteAdicional(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(IngredienteAdicional.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngredienteAdicionalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<IngredienteAdicional> rt = cq.from(IngredienteAdicional.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
