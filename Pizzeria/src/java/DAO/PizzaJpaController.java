/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DTO.Pizza;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.Sabor;
import DTO.Tipo;
import DTO.Pizzaadicional;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author MANUEL
 */
public class PizzaJpaController implements Serializable {

    public PizzaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pizza pizza) {
        if (pizza.getPizzaadicionalList() == null) {
            pizza.setPizzaadicionalList(new ArrayList<Pizzaadicional>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sabor idSabor = pizza.getIdSabor();
            if (idSabor != null) {
                idSabor = em.getReference(idSabor.getClass(), idSabor.getIdSabor());
                pizza.setIdSabor(idSabor);
            }
            Tipo idTipo = pizza.getIdTipo();
            if (idTipo != null) {
                idTipo = em.getReference(idTipo.getClass(), idTipo.getIdTipo());
                pizza.setIdTipo(idTipo);
            }
            List<Pizzaadicional> attachedPizzaadicionalList = new ArrayList<Pizzaadicional>();
            for (Pizzaadicional pizzaadicionalListPizzaadicionalToAttach : pizza.getPizzaadicionalList()) {
                pizzaadicionalListPizzaadicionalToAttach = em.getReference(pizzaadicionalListPizzaadicionalToAttach.getClass(), pizzaadicionalListPizzaadicionalToAttach.getPizzaadicionalPK());
                attachedPizzaadicionalList.add(pizzaadicionalListPizzaadicionalToAttach);
            }
            pizza.setPizzaadicionalList(attachedPizzaadicionalList);
            em.persist(pizza);
            if (idSabor != null) {
                idSabor.getPizzaList().add(pizza);
                idSabor = em.merge(idSabor);
            }
            if (idTipo != null) {
                idTipo.getPizzaList().add(pizza);
                idTipo = em.merge(idTipo);
            }
            for (Pizzaadicional pizzaadicionalListPizzaadicional : pizza.getPizzaadicionalList()) {
                Pizza oldPizzaOfPizzaadicionalListPizzaadicional = pizzaadicionalListPizzaadicional.getPizza();
                pizzaadicionalListPizzaadicional.setPizza(pizza);
                pizzaadicionalListPizzaadicional = em.merge(pizzaadicionalListPizzaadicional);
                if (oldPizzaOfPizzaadicionalListPizzaadicional != null) {
                    oldPizzaOfPizzaadicionalListPizzaadicional.getPizzaadicionalList().remove(pizzaadicionalListPizzaadicional);
                    oldPizzaOfPizzaadicionalListPizzaadicional = em.merge(oldPizzaOfPizzaadicionalListPizzaadicional);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pizza pizza) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza persistentPizza = em.find(Pizza.class, pizza.getIdPizza());
            Sabor idSaborOld = persistentPizza.getIdSabor();
            Sabor idSaborNew = pizza.getIdSabor();
            Tipo idTipoOld = persistentPizza.getIdTipo();
            Tipo idTipoNew = pizza.getIdTipo();
            List<Pizzaadicional> pizzaadicionalListOld = persistentPizza.getPizzaadicionalList();
            List<Pizzaadicional> pizzaadicionalListNew = pizza.getPizzaadicionalList();
            List<String> illegalOrphanMessages = null;
            for (Pizzaadicional pizzaadicionalListOldPizzaadicional : pizzaadicionalListOld) {
                if (!pizzaadicionalListNew.contains(pizzaadicionalListOldPizzaadicional)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pizzaadicional " + pizzaadicionalListOldPizzaadicional + " since its pizza field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idSaborNew != null) {
                idSaborNew = em.getReference(idSaborNew.getClass(), idSaborNew.getIdSabor());
                pizza.setIdSabor(idSaborNew);
            }
            if (idTipoNew != null) {
                idTipoNew = em.getReference(idTipoNew.getClass(), idTipoNew.getIdTipo());
                pizza.setIdTipo(idTipoNew);
            }
            List<Pizzaadicional> attachedPizzaadicionalListNew = new ArrayList<Pizzaadicional>();
            for (Pizzaadicional pizzaadicionalListNewPizzaadicionalToAttach : pizzaadicionalListNew) {
                pizzaadicionalListNewPizzaadicionalToAttach = em.getReference(pizzaadicionalListNewPizzaadicionalToAttach.getClass(), pizzaadicionalListNewPizzaadicionalToAttach.getPizzaadicionalPK());
                attachedPizzaadicionalListNew.add(pizzaadicionalListNewPizzaadicionalToAttach);
            }
            pizzaadicionalListNew = attachedPizzaadicionalListNew;
            pizza.setPizzaadicionalList(pizzaadicionalListNew);
            pizza = em.merge(pizza);
            if (idSaborOld != null && !idSaborOld.equals(idSaborNew)) {
                idSaborOld.getPizzaList().remove(pizza);
                idSaborOld = em.merge(idSaborOld);
            }
            if (idSaborNew != null && !idSaborNew.equals(idSaborOld)) {
                idSaborNew.getPizzaList().add(pizza);
                idSaborNew = em.merge(idSaborNew);
            }
            if (idTipoOld != null && !idTipoOld.equals(idTipoNew)) {
                idTipoOld.getPizzaList().remove(pizza);
                idTipoOld = em.merge(idTipoOld);
            }
            if (idTipoNew != null && !idTipoNew.equals(idTipoOld)) {
                idTipoNew.getPizzaList().add(pizza);
                idTipoNew = em.merge(idTipoNew);
            }
            for (Pizzaadicional pizzaadicionalListNewPizzaadicional : pizzaadicionalListNew) {
                if (!pizzaadicionalListOld.contains(pizzaadicionalListNewPizzaadicional)) {
                    Pizza oldPizzaOfPizzaadicionalListNewPizzaadicional = pizzaadicionalListNewPizzaadicional.getPizza();
                    pizzaadicionalListNewPizzaadicional.setPizza(pizza);
                    pizzaadicionalListNewPizzaadicional = em.merge(pizzaadicionalListNewPizzaadicional);
                    if (oldPizzaOfPizzaadicionalListNewPizzaadicional != null && !oldPizzaOfPizzaadicionalListNewPizzaadicional.equals(pizza)) {
                        oldPizzaOfPizzaadicionalListNewPizzaadicional.getPizzaadicionalList().remove(pizzaadicionalListNewPizzaadicional);
                        oldPizzaOfPizzaadicionalListNewPizzaadicional = em.merge(oldPizzaOfPizzaadicionalListNewPizzaadicional);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pizza.getIdPizza();
                if (findPizza(id) == null) {
                    throw new NonexistentEntityException("The pizza with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza pizza;
            try {
                pizza = em.getReference(Pizza.class, id);
                pizza.getIdPizza();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pizza with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Pizzaadicional> pizzaadicionalListOrphanCheck = pizza.getPizzaadicionalList();
            for (Pizzaadicional pizzaadicionalListOrphanCheckPizzaadicional : pizzaadicionalListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pizza (" + pizza + ") cannot be destroyed since the Pizzaadicional " + pizzaadicionalListOrphanCheckPizzaadicional + " in its pizzaadicionalList field has a non-nullable pizza field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sabor idSabor = pizza.getIdSabor();
            if (idSabor != null) {
                idSabor.getPizzaList().remove(pizza);
                idSabor = em.merge(idSabor);
            }
            Tipo idTipo = pizza.getIdTipo();
            if (idTipo != null) {
                idTipo.getPizzaList().remove(pizza);
                idTipo = em.merge(idTipo);
            }
            em.remove(pizza);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pizza> findPizzaEntities() {
        return findPizzaEntities(true, -1, -1);
    }

    public List<Pizza> findPizzaEntities(int maxResults, int firstResult) {
        return findPizzaEntities(false, maxResults, firstResult);
    }

    private List<Pizza> findPizzaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pizza.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pizza findPizza(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pizza.class, id);
        } finally {
            em.close();
        }
    }

    public int getPizzaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pizza> rt = cq.from(Pizza.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
