/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.NonexistentEntityException;
import DAO.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.IngredienteAdicional;
import DTO.Pizza;
import DTO.Pizzaadicional;
import DTO.PizzaadicionalPK;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author MANUEL
 */
public class PizzaadicionalJpaController implements Serializable {

    public PizzaadicionalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pizzaadicional pizzaadicional) throws PreexistingEntityException, Exception {
        if (pizzaadicional.getPizzaadicionalPK() == null) {
            pizzaadicional.setPizzaadicionalPK(new PizzaadicionalPK());
        }
        pizzaadicional.getPizzaadicionalPK().setIdIngrediente(pizzaadicional.getIngredienteAdicional().getIdIngrediente());
        pizzaadicional.getPizzaadicionalPK().setIdPizza(pizzaadicional.getPizza().getIdPizza());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngredienteAdicional ingredienteAdicional = pizzaadicional.getIngredienteAdicional();
            if (ingredienteAdicional != null) {
                ingredienteAdicional = em.getReference(ingredienteAdicional.getClass(), ingredienteAdicional.getIdIngrediente());
                pizzaadicional.setIngredienteAdicional(ingredienteAdicional);
            }
            Pizza pizza = pizzaadicional.getPizza();
            if (pizza != null) {
                pizza = em.getReference(pizza.getClass(), pizza.getIdPizza());
                pizzaadicional.setPizza(pizza);
            }
            em.persist(pizzaadicional);
            if (ingredienteAdicional != null) {
                ingredienteAdicional.getPizzaadicionalList().add(pizzaadicional);
                ingredienteAdicional = em.merge(ingredienteAdicional);
            }
            if (pizza != null) {
                pizza.getPizzaadicionalList().add(pizzaadicional);
                pizza = em.merge(pizza);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPizzaadicional(pizzaadicional.getPizzaadicionalPK()) != null) {
                throw new PreexistingEntityException("Pizzaadicional " + pizzaadicional + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pizzaadicional pizzaadicional) throws NonexistentEntityException, Exception {
        pizzaadicional.getPizzaadicionalPK().setIdIngrediente(pizzaadicional.getIngredienteAdicional().getIdIngrediente());
        pizzaadicional.getPizzaadicionalPK().setIdPizza(pizzaadicional.getPizza().getIdPizza());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizzaadicional persistentPizzaadicional = em.find(Pizzaadicional.class, pizzaadicional.getPizzaadicionalPK());
            IngredienteAdicional ingredienteAdicionalOld = persistentPizzaadicional.getIngredienteAdicional();
            IngredienteAdicional ingredienteAdicionalNew = pizzaadicional.getIngredienteAdicional();
            Pizza pizzaOld = persistentPizzaadicional.getPizza();
            Pizza pizzaNew = pizzaadicional.getPizza();
            if (ingredienteAdicionalNew != null) {
                ingredienteAdicionalNew = em.getReference(ingredienteAdicionalNew.getClass(), ingredienteAdicionalNew.getIdIngrediente());
                pizzaadicional.setIngredienteAdicional(ingredienteAdicionalNew);
            }
            if (pizzaNew != null) {
                pizzaNew = em.getReference(pizzaNew.getClass(), pizzaNew.getIdPizza());
                pizzaadicional.setPizza(pizzaNew);
            }
            pizzaadicional = em.merge(pizzaadicional);
            if (ingredienteAdicionalOld != null && !ingredienteAdicionalOld.equals(ingredienteAdicionalNew)) {
                ingredienteAdicionalOld.getPizzaadicionalList().remove(pizzaadicional);
                ingredienteAdicionalOld = em.merge(ingredienteAdicionalOld);
            }
            if (ingredienteAdicionalNew != null && !ingredienteAdicionalNew.equals(ingredienteAdicionalOld)) {
                ingredienteAdicionalNew.getPizzaadicionalList().add(pizzaadicional);
                ingredienteAdicionalNew = em.merge(ingredienteAdicionalNew);
            }
            if (pizzaOld != null && !pizzaOld.equals(pizzaNew)) {
                pizzaOld.getPizzaadicionalList().remove(pizzaadicional);
                pizzaOld = em.merge(pizzaOld);
            }
            if (pizzaNew != null && !pizzaNew.equals(pizzaOld)) {
                pizzaNew.getPizzaadicionalList().add(pizzaadicional);
                pizzaNew = em.merge(pizzaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PizzaadicionalPK id = pizzaadicional.getPizzaadicionalPK();
                if (findPizzaadicional(id) == null) {
                    throw new NonexistentEntityException("The pizzaadicional with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PizzaadicionalPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizzaadicional pizzaadicional;
            try {
                pizzaadicional = em.getReference(Pizzaadicional.class, id);
                pizzaadicional.getPizzaadicionalPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pizzaadicional with id " + id + " no longer exists.", enfe);
            }
            IngredienteAdicional ingredienteAdicional = pizzaadicional.getIngredienteAdicional();
            if (ingredienteAdicional != null) {
                ingredienteAdicional.getPizzaadicionalList().remove(pizzaadicional);
                ingredienteAdicional = em.merge(ingredienteAdicional);
            }
            Pizza pizza = pizzaadicional.getPizza();
            if (pizza != null) {
                pizza.getPizzaadicionalList().remove(pizzaadicional);
                pizza = em.merge(pizza);
            }
            em.remove(pizzaadicional);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pizzaadicional> findPizzaadicionalEntities() {
        return findPizzaadicionalEntities(true, -1, -1);
    }

    public List<Pizzaadicional> findPizzaadicionalEntities(int maxResults, int firstResult) {
        return findPizzaadicionalEntities(false, maxResults, firstResult);
    }

    private List<Pizzaadicional> findPizzaadicionalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pizzaadicional.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pizzaadicional findPizzaadicional(PizzaadicionalPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pizzaadicional.class, id);
        } finally {
            em.close();
        }
    }

    public int getPizzaadicionalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pizzaadicional> rt = cq.from(Pizzaadicional.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
