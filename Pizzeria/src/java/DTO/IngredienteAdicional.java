/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author MANUEL
 */
@Entity
@Table(name = "Ingrediente_Adicional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IngredienteAdicional.findAll", query = "SELECT i FROM IngredienteAdicional i")
    , @NamedQuery(name = "IngredienteAdicional.findByIdIngrediente", query = "SELECT i FROM IngredienteAdicional i WHERE i.idIngrediente = :idIngrediente")
    , @NamedQuery(name = "IngredienteAdicional.findByDescripcion", query = "SELECT i FROM IngredienteAdicional i WHERE i.descripcion = :descripcion")
    , @NamedQuery(name = "IngredienteAdicional.findByValor", query = "SELECT i FROM IngredienteAdicional i WHERE i.valor = :valor")})
public class IngredienteAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ingrediente")
    private Integer idIngrediente;
    @Column(name = "descripcion")
    private String descripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private Double valor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingredienteAdicional")
    private List<Pizzaadicional> pizzaadicionalList;

    public IngredienteAdicional() {
    }

    public IngredienteAdicional(Integer idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public Integer getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(Integer idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @XmlTransient
    public List<Pizzaadicional> getPizzaadicionalList() {
        return pizzaadicionalList;
    }

    public void setPizzaadicionalList(List<Pizzaadicional> pizzaadicionalList) {
        this.pizzaadicionalList = pizzaadicionalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIngrediente != null ? idIngrediente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IngredienteAdicional)) {
            return false;
        }
        IngredienteAdicional other = (IngredienteAdicional) object;
        if ((this.idIngrediente == null && other.idIngrediente != null) || (this.idIngrediente != null && !this.idIngrediente.equals(other.idIngrediente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.IngredienteAdicional[ idIngrediente=" + idIngrediente + " ]";
    }
    
}
