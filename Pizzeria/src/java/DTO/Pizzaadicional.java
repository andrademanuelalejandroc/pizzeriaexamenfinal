/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MANUEL
 */
@Entity
@Table(name = "Pizza_adicional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pizzaadicional.findAll", query = "SELECT p FROM Pizzaadicional p")
    , @NamedQuery(name = "Pizzaadicional.findByIdPizza", query = "SELECT p FROM Pizzaadicional p WHERE p.pizzaadicionalPK.idPizza = :idPizza")
    , @NamedQuery(name = "Pizzaadicional.findByIdIngrediente", query = "SELECT p FROM Pizzaadicional p WHERE p.pizzaadicionalPK.idIngrediente = :idIngrediente")
    , @NamedQuery(name = "Pizzaadicional.findByIdPizzaVenta", query = "SELECT p FROM Pizzaadicional p WHERE p.pizzaadicionalPK.idPizzaVenta = :idPizzaVenta")})
public class Pizzaadicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PizzaadicionalPK pizzaadicionalPK;
    @JoinColumn(name = "id_ingrediente", referencedColumnName = "id_ingrediente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private IngredienteAdicional ingredienteAdicional;
    @JoinColumn(name = "id_pizza", referencedColumnName = "id_pizza", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pizza pizza;

    public Pizzaadicional() {
    }

    public Pizzaadicional(PizzaadicionalPK pizzaadicionalPK) {
        this.pizzaadicionalPK = pizzaadicionalPK;
    }

    public Pizzaadicional(int idPizza, int idIngrediente, int idPizzaVenta) {
        this.pizzaadicionalPK = new PizzaadicionalPK(idPizza, idIngrediente, idPizzaVenta);
    }

    public PizzaadicionalPK getPizzaadicionalPK() {
        return pizzaadicionalPK;
    }

    public void setPizzaadicionalPK(PizzaadicionalPK pizzaadicionalPK) {
        this.pizzaadicionalPK = pizzaadicionalPK;
    }

    public IngredienteAdicional getIngredienteAdicional() {
        return ingredienteAdicional;
    }

    public void setIngredienteAdicional(IngredienteAdicional ingredienteAdicional) {
        this.ingredienteAdicional = ingredienteAdicional;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pizzaadicionalPK != null ? pizzaadicionalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pizzaadicional)) {
            return false;
        }
        Pizzaadicional other = (Pizzaadicional) object;
        if ((this.pizzaadicionalPK == null && other.pizzaadicionalPK != null) || (this.pizzaadicionalPK != null && !this.pizzaadicionalPK.equals(other.pizzaadicionalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.Pizzaadicional[ pizzaadicionalPK=" + pizzaadicionalPK + " ]";
    }
    
}
