/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author MANUEL
 */
@Embeddable
public class PizzaadicionalPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_pizza")
    private int idPizza;
    @Basic(optional = false)
    @Column(name = "id_ingrediente")
    private int idIngrediente;
    @Basic(optional = false)
    @Column(name = "id_pizza_venta")
    private int idPizzaVenta;

    public PizzaadicionalPK() {
    }

    public PizzaadicionalPK(int idPizza, int idIngrediente, int idPizzaVenta) {
        this.idPizza = idPizza;
        this.idIngrediente = idIngrediente;
        this.idPizzaVenta = idPizzaVenta;
    }

    public int getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(int idPizza) {
        this.idPizza = idPizza;
    }

    public int getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(int idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public int getIdPizzaVenta() {
        return idPizzaVenta;
    }

    public void setIdPizzaVenta(int idPizzaVenta) {
        this.idPizzaVenta = idPizzaVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPizza;
        hash += (int) idIngrediente;
        hash += (int) idPizzaVenta;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PizzaadicionalPK)) {
            return false;
        }
        PizzaadicionalPK other = (PizzaadicionalPK) object;
        if (this.idPizza != other.idPizza) {
            return false;
        }
        if (this.idIngrediente != other.idIngrediente) {
            return false;
        }
        if (this.idPizzaVenta != other.idPizzaVenta) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.PizzaadicionalPK[ idPizza=" + idPizza + ", idIngrediente=" + idIngrediente + ", idPizzaVenta=" + idPizzaVenta + " ]";
    }
    
}
