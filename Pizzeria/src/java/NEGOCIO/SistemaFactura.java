/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NEGOCIO;

import DAO.Conexion;
import javax.swing.JOptionPane;

/**
 *
 * @author MANUEL
 */
public class SistemaFactura {

    String[][] ingredientesPrimeraMitad;
    String[][] ingredientesPrimeraMitadCostos;
    String[][] ingredientesSegundaMitad;
    String[][] ingredientesSegundaMitadCostos;
    String[] saboresPrimeraMitad;
    String[] saboresSegundaMitad;
    String[] costoPizzas;
    String[] nombreDeLaPizza;
    String[] tamanoPizzas;
    double total;

    public SistemaFactura() {
    }

    public void guardaTamanoPizzas(Conexion con, String[] tamanoPorPizza) {

        DAO.TipoJpaController tipoDAO = new DAO.TipoJpaController(con.getBd());
        tamanoPizzas = new String[tamanoPorPizza.length];
        for (int i = 0; i < tamanoPorPizza.length; i++) {
            this.tamanoPizzas[i] = tipoDAO.findTipo(Integer.parseInt(tamanoPorPizza[i])).getDescripcion();
        }

    }

    public void calcularCostoPizza(String[] TamanoPorPizza, String[] saboresPrimeraMitad, String[] saboresSegundaMitad) {
        costoPizzas = new String[TamanoPorPizza.length];
        double valorPizza1 = 0;
        double valorPizza2 = 0;
        SistemaPizza sistema = new SistemaPizza();

        for (int i = 0; i < TamanoPorPizza.length; i++) {
            valorPizza1 = sistema.ObtenerPizza(Integer.parseInt((TamanoPorPizza[i])), Integer.parseInt(saboresPrimeraMitad[i])).getValor();
            if (!saboresSegundaMitad[i].equals("0")) {
                valorPizza2 = sistema.ObtenerPizza(Integer.parseInt(TamanoPorPizza[i]), Integer.parseInt(saboresSegundaMitad[i])).getValor();
            }

            costoPizzas[i] = Math.max(valorPizza1, valorPizza2) + "";
            sumarATotalFactura(Math.max(valorPizza1, valorPizza2));

        }
    }

    public void sumarATotalFactura(double subTotal) {
        this.total += subTotal;
    }

    // permite obtener el nombre de la pizza para la factura ejm: pizza mitad pollo y mitad jamon
    public void generarCadenaPizza() {

        this.nombreDeLaPizza = new String[this.saboresPrimeraMitad.length];

        for (int i = 0; i < this.saboresPrimeraMitad.length; i++) {
            String nombre = "";
            nombre += "Pizza " + tamanoPizzas[i];
            if (this.saboresSegundaMitad[i] == null) {
                nombre += " " + saboresPrimeraMitad[i];
            } else {
                nombre += " Mitad " + saboresPrimeraMitad[i] + " y Mitad " + saboresSegundaMitad[i];
            }
            nombreDeLaPizza[i] = nombre;
        }

    }

    public void guardarIngredientesPrimeraMitad(Conexion con, String[][] ingredientesPrimeraMitad) {

        DAO.IngredienteAdicionalJpaController ingredientesDAO = new DAO.IngredienteAdicionalJpaController(con.getBd());
        int cantidadIngredientes = ingredientesDAO.findIngredienteAdicionalEntities().size();
        this.ingredientesPrimeraMitad = new String[ingredientesPrimeraMitad.length][cantidadIngredientes];
        this.ingredientesPrimeraMitadCostos = new String[ingredientesPrimeraMitad.length][cantidadIngredientes];

        for (int i = 0; i < ingredientesPrimeraMitad.length; i++) {
            for (int j = 0; j < ingredientesPrimeraMitad[i].length; j++) {
                if (ingredientesPrimeraMitad[i][j] != null) {
                    this.ingredientesPrimeraMitad[i][j] = ingredientesDAO.findIngredienteAdicional(Integer.parseInt(ingredientesPrimeraMitad[i][j])).getDescripcion();
                    this.ingredientesPrimeraMitadCostos[i][j] = ingredientesDAO.findIngredienteAdicional(Integer.parseInt(ingredientesPrimeraMitad[i][j])).getValor() + "";
                    sumarATotalFactura(Double.parseDouble(ingredientesPrimeraMitadCostos[i][j]));
                }
            }
        }

    }

    public void guardarIngredientesSegundaMitad(Conexion con, String[][] ingredientesSegundaMitad) {

        DAO.IngredienteAdicionalJpaController ingredientesDAO = new DAO.IngredienteAdicionalJpaController(con.getBd());
        int cantidadIngredientes = ingredientesDAO.findIngredienteAdicionalEntities().size();
        this.ingredientesSegundaMitad = new String[ingredientesSegundaMitad.length][cantidadIngredientes];
        this.ingredientesSegundaMitadCostos = new String[ingredientesSegundaMitad.length][cantidadIngredientes];

        for (int i = 0; i < ingredientesSegundaMitad.length; i++) {

            for (int j = 0; j < ingredientesSegundaMitad[i].length; j++) {
                if (ingredientesSegundaMitad[i][j] != null) {
                    this.ingredientesSegundaMitad[i][j] = ingredientesDAO.findIngredienteAdicional(Integer.parseInt(ingredientesSegundaMitad[i][j])).getDescripcion();
                    this.ingredientesSegundaMitadCostos[i][j] = ingredientesDAO.findIngredienteAdicional(Integer.parseInt(ingredientesSegundaMitad[i][j])).getValor() + "";
                    sumarATotalFactura(Double.parseDouble(ingredientesSegundaMitadCostos[i][j]));
                }
            }
        }

    }

    public void guardarSaboresPrimeraMitad(Conexion con, String[] saboresPrimeraMitad) {

        DAO.SaborJpaController saboresDAO = new DAO.SaborJpaController(con.getBd());
        this.saboresPrimeraMitad = new String[saboresPrimeraMitad.length];

        for (int i = 0; i < saboresPrimeraMitad.length; i++) {
            int id = saboresPrimeraMitad[i].charAt(0) - '0';

            System.out.println(saboresPrimeraMitad[i] + " = " + saboresDAO.findSabor(id).getDescripcion());
            this.saboresPrimeraMitad[i] = saboresDAO.findSabor(id).getDescripcion();
        //    JOptionPane.showMessageDialog(null, saboresPrimeraMitad[i]);
        }
    }

    ;
    
  public void guardarSaboresSegundaMitad(Conexion con, String[] saboresSegundaMitad) {

        DAO.SaborJpaController saboresDAO = new DAO.SaborJpaController(con.getBd());
        this.saboresSegundaMitad = new String[saboresSegundaMitad.length];
        for (int i = 0; i < saboresSegundaMitad.length; i++) {
            if (!saboresSegundaMitad[i].equals("0")) {
                int id = saboresSegundaMitad[i].charAt(0) - '0';

                this.saboresSegundaMitad[i] = saboresDAO.findSabor(id).getDescripcion();
            }

        }
    }

    public void guardarDatos(String[] saboresPrimeraMitad, String[] saboresSegundaMitad, String[][] ingredientesPrimeraMitad, String[][] ingredientesSegundaMitad, String[] tamanoPizzas) {

        Conexion con = Conexion.getConexion();
    //    JOptionPane.showMessageDialog(null, "0");
        guardarSaboresPrimeraMitad(con, saboresPrimeraMitad);
    //    JOptionPane.showMessageDialog(null, "1");
        guardarIngredientesPrimeraMitad(con, ingredientesPrimeraMitad);
    //    JOptionPane.showMessageDialog(null, "2");
        guardarSaboresSegundaMitad(con, saboresSegundaMitad);
    //    JOptionPane.showMessageDialog(null, "3");
        guardarIngredientesSegundaMitad(con, ingredientesSegundaMitad);
   //     JOptionPane.showMessageDialog(null, "4");
        guardaTamanoPizzas(con, tamanoPizzas);

    }

    public String[][] getIngredientesPrimeraMitad() {
        return ingredientesPrimeraMitad;
    }

    public String[][] getIngredientesPrimeraMitadCostos() {
        return ingredientesPrimeraMitadCostos;
    }

    public String[][] getIngredientesSegundaMitad() {
        return ingredientesSegundaMitad;
    }

    public String[][] getIngredientesSegundaMitadCostos() {
        return ingredientesSegundaMitadCostos;
    }

    public String[] getSaboresPrimeraMitad() {
        return saboresPrimeraMitad;
    }

    public String[] getSaboresSegundaMitad() {
        return saboresSegundaMitad;
    }

    public String[] getCostoPizzas() {
        return costoPizzas;
    }

    public String[] getNombreDeLaPizza() {
        return nombreDeLaPizza;
    }

    public String[] getTamanoPizzas() {
        return tamanoPizzas;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
