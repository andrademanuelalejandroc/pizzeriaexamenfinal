/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NEGOCIO;

import DAO.Conexion;
import DTO.Pizza;
import DTO.Pizzaadicional;
import java.util.List;


/**
 *
 * @author MANUEL
 */
public class SistemaPizza {

    public SistemaPizza() {
    }

    public String[] obtenerIdSabores() {
        Conexion con = Conexion.getConexion();

        DAO.SaborJpaController saborDAO = new DAO.SaborJpaController(con.getBd());
        List<DTO.Sabor> list = saborDAO.findSaborEntities();
        String[] sabores = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            sabores[i] = list.get(i).getIdSabor() + "";
        }
        return sabores;
    }

    public void guardarIngredientes(String[] sabores, String[][] ingredientes,int[] idPedidos) throws Exception {
        Conexion con = Conexion.getConexion();
       
        DAO.PizzaJpaController pizzaDAO = new DAO.PizzaJpaController(con.getBd());
        DAO.IngredienteAdicionalJpaController ingredienteAdicionalDAO = new DAO.IngredienteAdicionalJpaController(con.getBd());
        DAO.PizzaadicionalJpaController pizzaAdicionDAO = new DAO.PizzaadicionalJpaController(con.getBd());
        
        for (int i = 0; i < sabores.length; i++) {
            if (!sabores[i].equals("0")) {
     
                
                for (int j = 0; j < ingredientes[i].length; j++) {
                    if (ingredientes[i][j] != null) {

                        DTO.Pizza pizza = pizzaDAO.findPizza(Integer.parseInt(sabores[i]));
                        DTO.IngredienteAdicional ingrediente = ingredienteAdicionalDAO.findIngredienteAdicional(Integer.parseInt(ingredientes[i][j]));
                        DTO.Pizzaadicional pizzaAdicional = new DTO.Pizzaadicional();
                        DTO.PizzaadicionalPK pizzaadicionalPK = new DTO.PizzaadicionalPK(pizza.getIdPizza(), ingrediente.getIdIngrediente(),idPedidos[i]);
                         pizzaAdicional.setIngredienteAdicional(ingrediente);
                        pizzaAdicional.setPizza(pizza);
                        pizzaAdicional.setPizzaadicionalPK(pizzaadicionalPK);
                        pizzaAdicionDAO.create(pizzaAdicional);

                    }
                }
            }
        }

    }

    public int[] obtenerIdPedidos(int cantidadPizzas) {

        int ultimoPedido = ObtenerIdUltimoPedido() + 1;
        int pedidos[] = new int[cantidadPizzas];

        for (int i = 0; i < cantidadPizzas; i++) {
            pedidos[i] = ultimoPedido;
            ultimoPedido++;
        }

        return pedidos;

    }

    public int ObtenerIdUltimoPedido() {

        Conexion con = Conexion.getConexion();
        DAO.PizzaadicionalJpaController PizzaAdicionDAO = new DAO.PizzaadicionalJpaController(con.getBd());

        List<DTO.Pizzaadicional> list = PizzaAdicionDAO.findPizzaadicionalEntities();

        if (list == null) {
            return 0;
        }
        int ultimoPedido = 0;
        for (Pizzaadicional pizzaadicional : list) {
            if (pizzaadicional.getPizzaadicionalPK().getIdPizzaVenta() > ultimoPedido) {
                ultimoPedido = pizzaadicional.getPizzaadicionalPK().getIdPizzaVenta();
            }
        }

        return ultimoPedido;
    }

    public Pizza ObtenerPizza(int tipo, int sabor) {
        Conexion con = Conexion.getConexion();
        DAO.PizzaJpaController pizzaDAO = new DAO.PizzaJpaController(con.getBd());
        List<Pizza> pizzas = pizzaDAO.findPizzaEntities();
        Pizza pizza = new Pizza();
        for (Pizza pizza1 : pizzas) {
            if (pizza1.getIdSabor().getIdSabor() == sabor && pizza1.getIdTipo().getIdTipo() == tipo) {
                pizza = pizza1;
            }
        }
        return pizza;
    }

    public List<DTO.Tipo> obtenerTipos() {
        Conexion con = Conexion.getConexion();
        DAO.TipoJpaController tipoDAO = new DAO.TipoJpaController(con.getBd());
        List<DTO.Tipo> list = tipoDAO.findTipoEntities();
        return list;
    }

    public String[] obtenerDescripcionSabores() {
        Conexion con = Conexion.getConexion();
        DAO.SaborJpaController saborDAO = new DAO.SaborJpaController(con.getBd());
        List<DTO.Sabor> sabores = saborDAO.findSaborEntities();
        String[] descripcion = new String[sabores.size()];

        for (int i = 0; i < descripcion.length; i++) {
            descripcion[i] = sabores.get(i).getDescripcion();
        }
        return descripcion;

    }

    public List<DTO.Sabor> obtenerSabores() {
        Conexion con = Conexion.getConexion();
        DAO.SaborJpaController saborDAO = new DAO.SaborJpaController(con.getBd());
        List<DTO.Sabor> list = saborDAO.findSaborEntities();
        return list;
    }

    public List<DTO.IngredienteAdicional> ObtenerIngredientes() {
        Conexion con = Conexion.getConexion();
        DAO.IngredienteAdicionalJpaController ingredientesDAO = new DAO.IngredienteAdicionalJpaController(con.getBd());
        List<DTO.IngredienteAdicional> list = ingredientesDAO.findIngredienteAdicionalEntities();
        return list;
    }

    public String[] obtenerImagenes() {
        Conexion con = Conexion.getConexion();

        DAO.SaborJpaController saborDAO = new DAO.SaborJpaController(con.getBd());
        List<DTO.Sabor> sabores = saborDAO.findSaborEntities();
        String[] imagenes = new String[sabores.size()];

        for (int i = 0; i < imagenes.length; i++) {
            imagenes[i] = sabores.get(i).getUrlImagen();
        }

        return imagenes;
    }

}
