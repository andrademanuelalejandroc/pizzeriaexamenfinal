<%-- 
    Document   : index
    Created on : 18/12/2020, 08:51:26 PM
    Author     : MANUEL
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Mi pizzeria</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />

    </head>
    <!-- body -->

    <body>
        <!-- Header-->
        <header>
            <div class="row justify-content-between text-center">
                <div class="col-2 icon offset-2">
                    <a href="index.jsp"><img class="img-fluid" src="images/logoPizzeria.jpg" alt="" /></a>
                </div>
                <div class=" col-4 align-self-center">
                    <h1>Mi pizzeria</h1>
                    <br />
                    <h6>Realizado por Manuel Coronel</h6>
                    <h6>Codigos : 1151806</h6>
                </div>
            </div>
        </header>
        <!-- fin Header-->

        <!-- cuerpo -->
        <!-- digitar cantidad de pizzas -->

        <form action="ControladorPizza" method="GET">
            <div class="container digitBox border rounded p-3">
                <div class="row digitar  justify-content-between">

                    <div class="col-3 offset-1 align-self-center">
                        <label for="cantidadPizzas">Digite cantidad de Pizzas</label>
                    </div>
                    <div class="col-2 align-self-center">
                        <input class="form-control" type="number" value="1" min="1" name="cantidadPizzas"  id="cantidadPizzas">
                    </div>
                    <div class="col-3">
                        <div class="row">
                            <div class="col">
                                <%String disable ="";
                                 if(request.getSession().getAttribute("cantidadDePizza") != null){disable = "disabled";}
                                %>
                                <input type="submit" class="btn btn-primary" data-toggle="button" name="accion" value="Crear" aria-pressed="false" <%=disable%> >
                                
                         </div>
                    </div>
                </div><%
                    if(request.getSession().getAttribute("cantidadDePizza") != null){%>
                         <div class="col-3">
                        <div class="row">
                            <div class="col">
                                
                                          <a   class="btn btn-primary" href="ControladorPizza?accion=actualizarIndex">Carga de nuevo</a>
                         </div>
                    </div>
                </div>
                    <%}%>
                    
            </div>     </div>
        </form>

        <%

            if (request.getSession().getAttribute("cantidadDePizza") != null) {
                int cantidad = Integer.parseInt(request.getSession().getAttribute("cantidadDePizza") + "");
                System.out.println(cantidad);


        %>
            
        <form action="ControladorPizza" method="GET">
            

        <div class="container justify-content-center">

            <%         for (int i = 0; i < cantidad; i++) {
                    List<DTO.Tipo> list = (List<DTO.Tipo>) request.getSession().getAttribute("tipos");

            %>

            <div class="row border rounded p-3" style="margin-top:20px;">
                <div class="col-3 offset-1 align-self-center">
                    <label>Tamaño de pizza <%=i + 1%></label>
                </div>
                <div class="col-3 align-self-center">
                    <select name="tama" class="form-control"  >
                        <% for (int j = 0; j < list.size(); j++) {%>    
                        <option value="<%=list.get(j).getIdTipo()%>"><%=list.get(j).getDescripcion()%></option>
                        <%}%>
                    </select>
                </div>
            </div>
                    
           


            <%

                }
            %>  </div>
            <br>
                        <div class="col-3 offset-8 align-self-center">   
                  <input type="submit" class="btn btn-primary" data-toggle="button" name="accion" value="Cargar opciones" aria-pressed="false"  >
                        </div>
                     </form>
            <%  }

            %>

        <!-- fin digitar cantidad de pizzas -->
        <!-- tamaño de las pizzas -->
    
        <!-- tamaño de las pizzas -->
        <!-- fin cuerpo -->

        <!-- Optional JavaScript -->
        <script src="js/pizzas.js" charset="utf-8"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <!-- fin body -->
    </body>

</html>
