

function cambiarPizza(idSelect, idCiclo, imagenes, sabores, cantidadIngredientes, saboresId) {

    var id = (document.getElementById("nombresPizzas" + idSelect + idCiclo).value);
    id--;
    cargarImagen(idSelect, idCiclo, imagenes, id);
    cargarSaborDeLaPizza(idSelect, idCiclo, sabores, id);

    if (idSelect == 2 && id != -1) {
        habilitarIngredientes(idCiclo, cantidadIngredientes);
    } else {
        if (idSelect == 2 && id == -1)
            deshabilitarIngredientes(idCiclo, cantidadIngredientes);
    }

    cargaListaDeSaboresDeLaPizza(idSelect, idCiclo, sabores, id, saboresId);
}

function habilitarIngredientes(idCiclo, cantidadIngredientes) {

    for (var i = 0; i < cantidadIngredientes; i++) {
        var x = document.getElementById('box' + idCiclo + i);
        x.disabled = false;
    }
}

function deshabilitarIngredientes(idCiclo, cantidadIngredientes) {

    for (var i = 0; i < cantidadIngredientes; i++) {
        var x = document.getElementById('box' + idCiclo + i);
        x.disabled = true;
        x.checked = false;
    }
}

function cargarImagen(idSelect, idCiclo, imagenes, id) {

    imagenes = imagenes.replace('[', '');
    imagenes = imagenes.replace(']', '');
    var imagenesSplit = imagenes.split(',', );
    var nombreURL = `<img style="width:370px; height:140px;"  class="img-fluid" src="${imagenesSplit[id]}">`;
    if (id == -1)
        nombreURL = '';
    document.getElementById("pizzaImagen" + idSelect + idCiclo).innerHTML = nombreURL;
}



function cargarSaborDeLaPizza(idSelect, idCiclo, sabores, id) {

    sabores = sabores.replace('[', '');
    sabores = sabores.replace(']', '');
    sabores = sabores.split(',');
    var sabor = `Ingrediente adicionales (Pizza ${sabores[id]})`;
    if (id == -1) {
        sabor = `Ingrediente adicionales (Escogio ninguno)`;
    }
    document.getElementById('nombrePizza' + idSelect + idCiclo).innerHTML = sabor;

}

function cargaListaDeSaboresDeLaPizza(idSelect, idCiclo, sabores, id, saboresId) {

    sabores = sabores.replace('[', '');
    sabores = sabores.replace(']', '');
    sabores = sabores.split(',');

    saboresId = saboresId.replace('[', '');
    saboresId = saboresId.replace(']', '');
    saboresId = saboresId.split(',');

    var lista = '';
    if (idSelect == 2) {
        idSelect = 1;
    } else {
        lista += '<option value="0" >Ninguno</option>'
        idSelect = 2;
    }
    var id2 = (document.getElementById("nombresPizzas" + idSelect + idCiclo).value);
    id2--;
    for (var i = 0; i < sabores.length; i++) {
        if (id != i) {
            if (i == id2) {
                lista += `<option  value="${i+1}" id="select"${idSelect + "" + idCiclo} selected="" >${sabores[i]}</option>`;
            } else {
                lista += `<option  value="${i+1}" id="select"${idSelect + "" + idCiclo} >${sabores[i]}</option>`;
            }

        }
    }
    document.getElementById("nombresPizzas" + idSelect + idCiclo).innerHTML = lista;

}