<%-- 
    Document   : opciones
    Created on : 18/12/2020, 09:11:52 PM
    Author     : MANUEL
--%>

<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
            />
        <title>Opciones</title>
        <link rel="stylesheet" href="../css/header.css" />

        <!-- Bootstrap CSS -->
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
            integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
            crossorigin="anonymous"
            />
    </head>

    <body>
        <!-- Header-->
        <header>
            <div class="row justify-content-between text-center">
                <div class="col-2 icon offset-2">
                    <a href="../index.jsp"><img class="img-fluid" src="../images/logoPizzeria.jpg" alt="" /></a>
                </div>
                <div class="col-4 align-self-center">
                    <h1>Mi pizzeria</h1>
                    <br />
                    <h6>Realizado por Manuel Coronel</h6>
                    <h6>Codigo : 1151806</h6>
                </div>
            </div>
        </header>
        <!-- fin Header-->
        <!-- cuerpo -->
        <div class="container justify-content-center">
            <form action="../ControladorFactura" method="POST">

                <%int cantidadPizza = Integer.parseInt(request.getSession().getAttribute("cantidadDePizza") + "");
                    List<DTO.Sabor> sabores = (List<DTO.Sabor>) request.getSession().getAttribute("sabores");
                    List<DTO.IngredienteAdicional> ingredientes = (List<DTO.IngredienteAdicional>) request.getSession().getAttribute("ingredientes");
                    for (int i = 0; i < cantidadPizza; i++) {
                %>


                <div class="container row border rounded mt-5 pt-4 pb-4 pr-1 pl-1">
                    <div class="col-6">
                        <div  class="row">
                            <div class="col-12">
                                <p class="mb-5">Escoja sabor para pizza <%=i + 1%>  (puede escoger uno o dos):</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label id="nombrePizza1<%=i%>">Ingrediente adicionales (Pizza <%=sabores.get(0).getDescripcion()%>)</label><br>
                                <%
                                    for (int j = 0; j < ingredientes.size(); j++) {

                                %>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" value='<%=ingredientes.get(j).getIdIngrediente()%>' name="ingredientes1<%=i%>">
                                    <label style="margin:10px;" class="form-check-label"><%=ingredientes.get(j).getDescripcion()%></label> </div>
                                    <%
                                        }
                                    %>
                            </div>      
                        </div>
                        <div class='row'  >
                            <div class="col-12">
                                <label id="nombrePizza2<%=i%>">Ingrediente adicionales (Escogio ninguno)</label><br>

                                <%
                                    for (int j = 0; j < ingredientes.size(); j++) {
                                %>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" id='box<%=i%><%=j%>'value="<%=ingredientes.get(j).getIdIngrediente()%>" type="checkbox" name="ingredientes2<%=i%>"  disabled>
                                    <label style="margin:10px;" class="form-check-label"><%=ingredientes.get(j).getDescripcion()%></label> </div>
                                    <%
                                        }
                                    %>
                            </div>  
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row">
                            <div class="col">

                                <select class="form-control" name="select1" id="nombresPizzas1<%=i%>" onchange="cambiarPizza(1,<%=i%>, '${Arrays.toString(imagenes)}', '${Arrays.toString(descripciones)}',<%=ingredientes.size()%>,'${Arrays.toString(idSabores)}')">
                                    <option value="<%=sabores.get(0).getIdSabor()%>" ><%=sabores.get(0).getDescripcion()%></option>
                                    <%
                                        for (int u = 1; u < sabores.size(); u++) {
                                    %>
                                    <option value="<%=sabores.get(u).getIdSabor()%>" id="select1<%=i%>"><%=sabores.get(u).getDescripcion()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control" name="select2" id="nombresPizzas2<%=i%>" onchange="cambiarPizza(2,<%=i%>, '${Arrays.toString(imagenes)}', '${Arrays.toString(descripciones)}',<%=ingredientes.size()%>,'${Arrays.toString(idSabores)}')">
                                    <option value="0">Ninguno</option>
                                    <%
                                        for (int u = 1; u < sabores.size(); u++) {
                                    %>
                                    <option value="<%=sabores.get(u).getIdSabor()%>" id="select2<%=i%>" ><%=sabores.get(u).getDescripcion()%></option>
                                    <%}%>
                                </select>
                            </div>

                        </div>
                        <div class="row justify-content-center mt-5">

                            <div  id="pizzaImagen1<%=i%>"class="col-6">
                                <img class="img-fluid" src="<%=sabores.get(0).getUrlImagen()%>" alt="">
                            </div>

                            <div class="col-6" id="pizzaImagen2<%=i%>">
                            </div>
                        </div>
                    </div>
                </div>
                <input style="display:none;" type="text" name="tama" value="${tama[i]}">



                <%              }
                %>
                <br><br>
                <input style="margin-left: 45%" type="submit" class="btn btn-primary" data-toggle="button" name="accion" value="Calcular factura" aria-pressed="false"  >

            </form>
        </div>

        <!-- fin cuerpo -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="../js/pizzas.js" charset="utf-8"></script>
        <script

            src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"
        ></script>
    </body>
</html>